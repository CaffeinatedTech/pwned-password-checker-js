# Pwned Password Checker JS

Checks passwords against the Have I Been Pwned API to see if they have been compromised.  Without revealing the password.  The API is detailed in Troy Hunt's blog post https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/